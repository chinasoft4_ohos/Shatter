﻿#  Shatter



#### 项目介绍
- 项目名称： Shatter

- 所属系列：openharmony的第三方组件适配移植

- 功能：实现划分ui模块的库。

- 项目移植状态：主功能完成

- 调用差异：无

- 开发版本：sdk6，DevEco Studio2.2 Beta1

- 基线版本：Release 1.0.8

  

#### 效果演示

<img src="gif/ShatterDemo.gif"></img>


#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
 ```
2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:Shatter:1.0.0')
    ......  
 }
 ```

在sdk6，DevEco Studio2.2 Beta1 下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

方式一：让shatter有监听AbilitySlice全部生命周期的能力，在BaseAbilitySlice实现ShatterAbilitySliceInterface，并复写你需要被shatter感知的生命周期,如：

```
public class BaseAbilitySlice extends AbilitySlice implements ShatterAbilitySliceInterface {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    private ShatterManager mShatterManager;

    /**
     * getShatterManager
     *
     * @return ShatterManager
     */
    public ShatterManager getShatterManager() {
        if (mShatterManager == null) {
            mShatterManager = new ShatterManager(this);
        }
        return mShatterManager;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        HiLog.info(LABEL, "BaseAbilitySlice onStart() called");
        dispatch(LifeEvent.ON_START, intent);
    }

    @Override
    public void onActive() {
        super.onActive();
        dispatch(LifeEvent.ON_ACTIVE);
    }

    //...

    /**
     * 分发生命周期事件
     *
     * @param event
     * @param args
     */
    protected void dispatch(String event, Object... args) {
        HiLog.info(LABEL, getClass().getSimpleName() + " dispatch " + event + "()  --");
        MethodExecutor.scheduleMethod(event, getShatterManager(), args);
    }
}

```



方式二：仅仅需要监听部分生命周期，在baseAbilitySlice中的onCreate()中写上如下语句
```java
public class BottomShatter extends Shatter {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    private static final String TAG = "BottomShatter";
    private static final int REQUEST_CODE = 100;

    private Button mBottomBtn;

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    protected int getLayoutResId() {
        return kale.ui.ResourceTable.Layout_shatter_bottom;
    }

    @Override
    public void onBindComponent(Component rootView) {
        mBottomBtn = (Button) findViewById(kale.ui.ResourceTable.Id_btn_start);
        mBottomBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent secondIntent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("kale.ui")
                        .withAbilityName("kale.ui.ability.PagerAbility")
                        .build();
                secondIntent.setOperation(operation);
                startAbilityForResult(secondIntent, REQUEST_CODE);
            }
        });
    }

    @Override
    public void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        HiLog.info(LABEL, "bottom onAbilityResult() called with: requestCode = [" + requestCode + "], resultCode = ["
                + resultCode + "], data = [" + data + "]");
    }
}

```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
Copyright 2016-2019 Jack Tony

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```