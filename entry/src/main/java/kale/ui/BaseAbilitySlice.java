/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui;

import kale.ui.shatter.ShatterAbilitySliceInterface;
import kale.ui.shatter.ShatterManager;
import kale.ui.shatter.lifecycle.LifeEvent;
import kale.ui.shatter.lifecycle.MethodExecutor;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 使用Shatter AbilitySlice 的基类
 *
 * @since 2021-05-20
 */
public class BaseAbilitySlice extends AbilitySlice implements ShatterAbilitySliceInterface {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    private ShatterManager mShatterManager;

    /**
     * getShatterManager
     *
     * @return ShatterManager
     */
    public ShatterManager getShatterManager() {
        if (mShatterManager == null) {
            mShatterManager = new ShatterManager(this);
        }
        return mShatterManager;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        HiLog.info(LABEL, "BaseAbilitySlice onStart() called");
        dispatch(LifeEvent.ON_START, intent);
    }

    @Override
    public void onActive() {
        super.onActive();
        dispatch(LifeEvent.ON_ACTIVE);
    }

    @Override
    public void onInactive() {
        super.onInactive();
        dispatch(LifeEvent.ON_INACTIVE);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        dispatch(LifeEvent.ON_FOREGROUND, intent);
    }

    @Override
    public void onBackground() {
        super.onBackground();
        dispatch(LifeEvent.ON_BACKGROUND);
    }

    @Override
    public void onStop() {
        super.onStop();
        dispatch(LifeEvent.ON_STOP);
    }

    @Override
    public void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        dispatch(LifeEvent.ON_ABILITY_RESULT, requestCode, resultCode, resultData);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dispatch(LifeEvent.ON_BACK_PRESSED);
    }

    /**
     * 分发生命周期事件
     *
     * @param event
     * @param args
     */
    protected void dispatch(String event, Object... args) {
        HiLog.info(LABEL, getClass().getSimpleName() + " dispatch " + event + "()  --");
        MethodExecutor.scheduleMethod(event, getShatterManager(), args);
    }
}
