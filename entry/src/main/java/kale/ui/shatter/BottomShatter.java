package kale.ui.shatter;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 底部shatter
 *
 * @author xwg
 * @since 2015-09-01
 */
public class BottomShatter extends Shatter {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    private static final String TAG = "BottomShatter";
    private static final int REQUEST_CODE = 100;

    private Button mBottomBtn;

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    protected int getLayoutResId() {
        return kale.ui.ResourceTable.Layout_shatter_bottom;
    }

    @Override
    public void onBindComponent(Component rootView) {
        mBottomBtn = (Button) findViewById(kale.ui.ResourceTable.Id_btn_start);
        mBottomBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent secondIntent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getAbilitySlice().getBundleName())
                        .withAbilityName("kale.ui.ability.PagerAbility")
                        .build();
                secondIntent.setOperation(operation);
                startAbilityForResult(secondIntent, REQUEST_CODE);
            }
        });
    }

    @Override
    public void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        HiLog.info(LABEL, "bottom onAbilityResult() called with: requestCode = [" + requestCode + "], resultCode = ["
                + resultCode + "], data = [" + data + "]");
    }
}
