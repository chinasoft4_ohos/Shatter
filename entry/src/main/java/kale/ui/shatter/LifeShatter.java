package kale.ui.shatter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 生命周期事件测试类
 *
 * @since 2017-10-23
 */
public class LifeShatter extends Shatter {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    private static final String TAG = "LifeShatter";

    @Override
    protected int getLayoutResId() {
        return NO_LAYOUT;
    }

    @Override
    protected void onBindComponent(Component rootView) {
    }

    @Override
    protected void onAttach(AbilitySlice abilityslice) {
        super.onAttach(abilityslice);
        HiLog.info(LABEL, "onAttach() called with: mAbilityslice = [" + abilityslice + "]");
    }

    @Override
    public void onActStart(Intent intent) {
        super.onActStart(intent);
        HiLog.info(LABEL, "onStart() called");
    }

    @Override
    public void onActStop() {
        HiLog.info(LABEL, "onStop() called");
    }

    @Override
    public void onActActive() {
        super.onActActive();
        HiLog.info(LABEL, "onActActive() called");
    }

    @Override
    public void onActInactive() {
        super.onActInactive();
        HiLog.info(LABEL, "onActInactive() called");
    }

    @Override
    public void onActBackground() {
        super.onActBackground();
        HiLog.info(LABEL, "onActBackground() called");
    }

    @Override
    public void onActBackPressed() {
        super.onActBackPressed();
        HiLog.info(LABEL, "onActBackPressed() called");
    }

    @Override
    public void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        HiLog.info(LABEL, "onAbilityResult() called with: requestCode = ["
                + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
    }
}
