package kale.ui.shatter;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.TextAlignment;

/**
 * 中间的shatter
 *
 * @author xwg
 * @since 2015-09-22
 */
public class MiddleShatter extends Shatter {
    @Override
    protected int getLayoutResId() {
        return kale.ui.ResourceTable.Layout_shatter_middle;
    }

    @Override
    public void onBindComponent(Component rootView) {
        getShatterManager().add(kale.ui.ResourceTable.Id_sl_inner, new InnerShatter());
    }

    /**
     * 中间的shatter
     *
     * @author xwg
     * @since 2015-09-22
     */
    public static class InnerShatter extends Shatter {
        private static final int TEXT_SIZE = 20;
        private final int numtwenty = 20;
        private final int numthirty = 30;
        private final int numsixtyeight = 68;
        private final int numR = 217;
        private final int numG = 234;
        private final int numB = 211;

        @Override
        protected int getLayoutResId() {
            return 0;
        }

        @Override
        public void onBindComponent(Component rootView) {
            rootView.setBackground(setShape());

            if (rootView instanceof StackLayout) {
                Text text = new Text(rootView.getContext());
                text.setTextSize(TEXT_SIZE, Text.TextSizeType.VP);
                text.setText("A Inner Shatter :-)");
                text.setTextAlignment(TextAlignment.CENTER);

                ((StackLayout) rootView).addComponent(text, ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_PARENT);
            }
        }

        private ShapeElement setShape() {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(numR,numG,numB));
            shapeElement.setShape(ShapeElement.RECTANGLE);
            shapeElement.setCornerRadius(numtwenty);
            shapeElement.setDashPathEffectValues(new float[]{numthirty, numthirty}, 1);
            shapeElement.setStroke(1, new RgbColor(numsixtyeight, numsixtyeight, numsixtyeight));
            return shapeElement;
        }
    }
}
