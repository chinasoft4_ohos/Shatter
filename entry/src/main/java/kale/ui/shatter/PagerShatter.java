package kale.ui.shatter;

import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * PagerShatter 有生命周期的組件
 *
 * @author xwg
 * @since 2015-11-21
 */
public class PagerShatter extends Shatter {
    private static final int TEXT_SIZE = 30;
    private Text text;

    @Override
    protected int getLayoutResId() {
        return kale.ui.ResourceTable.Layout_shatter_pager;
    }

    @Override
    protected void onBindComponent(Component rootView) {
        text = (Text) findViewById(kale.ui.ResourceTable.Id_text_pager);
        text.setTextSize(TEXT_SIZE, Text.TextSizeType.VP);
    }

    /**
     * handleData
     *
     * @param data
     */
    public void handleData(String data) {
        text.setText(data);
    }

    public String getText() {
        return text.getText();
    }
}
