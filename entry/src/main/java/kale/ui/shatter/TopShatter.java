package kale.ui.shatter;

import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * 顶部shatter
 *
 * @since 2021-05-21
 */
public class TopShatter extends Shatter {
    private Text mTopTv;

    @Override
    protected int getLayoutResId() {
        return NO_LAYOUT;
    }

    @Override
    public void onBindComponent(Component rootView) {
        mTopTv = (Text) findViewById(kale.ui.ResourceTable.Id_text_top);
        mTopTv.append("  :)");
    }
}
