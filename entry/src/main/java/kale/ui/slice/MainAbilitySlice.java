/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.slice;

import kale.ui.BaseAbilitySlice;
import kale.ui.ResourceTable;
import kale.ui.shatter.LifeShatter;
import kale.ui.shatter.MiddleShatter;
import kale.ui.shatter.TopShatter;
import kale.ui.shatter.BottomShatter;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

/**
 * MainAbilitySlice
 *
 * @since 2021-05-02
 */
public class MainAbilitySlice extends BaseAbilitySlice {
    private DirectionalLayout mTopll;
    private StackLayout mMiddlefl;
    private StackLayout mBottomfl;
    private final int numforty = 48;
    private final int numsixty = 62;
    private final int numonehundred = 157;
    private final int numtwenty = 20;
    private final int numthirty = 30;
    private final int numsixtyeight = 68;

    @Override
    public void onStart(Intent intent) {
        super.setUIContent(ResourceTable.Layout_ability_main);
        init();
        WindowManager.getInstance().getTopWindow().get().
                setStatusBarColor(Color.rgb(numforty, numsixty, numonehundred)); // 设置状态栏颜色
        getShatterManager()
                .add(ResourceTable.Id_dl_root, new LifeShatter())
                .add(ResourceTable.Id_dl_top, new TopShatter())
                .add(ResourceTable.Id_sl_middle, new MiddleShatter())
                .add(ResourceTable.Id_sl_bottom, new BottomShatter());
        super.onStart(intent);
    }

    /**
     * 初始化
     */
    public void init() {
        mTopll = (DirectionalLayout)findComponentById(ResourceTable.Id_dl_top);
        mMiddlefl = (StackLayout) findComponentById(ResourceTable.Id_sl_middle);
        mBottomfl = (StackLayout) findComponentById(ResourceTable.Id_sl_bottom);
        mTopll.setBackground(setShape());
        mMiddlefl.setBackground(setShape());
        mBottomfl.setBackground(setShape());
        WindowManager.getInstance().getTopWindow().get().
                setStatusBarColor(Color.rgb(numforty, numsixty, numonehundred)); // 设置状态栏颜色
    }

    private ShapeElement setShape() {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setCornerRadius(numtwenty);
        shapeElement.setDashPathEffectValues(new float[]{numthirty, numthirty}, 1);
        shapeElement.setStroke(1, new RgbColor(numsixtyeight, numsixtyeight, numsixtyeight));
        return shapeElement;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
