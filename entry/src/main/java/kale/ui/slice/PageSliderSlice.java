/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.slice;

import kale.ui.BaseAbilitySlice;
import kale.ui.ResourceTable;
import kale.ui.component.ScalpelFrameLayout;
import kale.ui.shatter.PagerShatter;
import kale.ui.shatter.Shatter;
import kale.ui.shatter.ShatterManager;
import kale.ui.shatter.adapter.ShatterPagerAdapter;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AbsButton;
import ohos.agp.components.ComponentState;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Switch;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

/**
 * PageSliderSlice
 *
 * @since 2021-05-20
 */
public class PageSliderSlice extends BaseAbilitySlice {
    final String[] data = new String[]{"英国", "法国", "爱尔兰", "荷兰", "比利时", "卢森堡", "摩纳哥", "泽西", "捷克", "斯洛伐克", "斯洛文尼亚", "德国"};
    private PageSlider pageSlider;
    private final int numforty = 48;
    private final int numsixty = 62;
    private final int numonehundred = 157;
    private final int numfifty = 50;
    private final int mOnColor = 0xFF9B9B9B;
    private final int mOffColor = 0xFFE2E2E2;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_pager);
        WindowManager.getInstance().getTopWindow().get().
                setStatusBarColor(Color.rgb(numforty, numsixty, numonehundred)); // 设置状态栏颜色

        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);
        pageSlider.setEnabled(true);
        pageSlider.setPageCacheSize(2);
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int index) {
                if (index == PageSlider.SLIDING_STATE_IDLE && pageSlider.getCurrentPage() >= 0) {
                    pageSlider.setTag(data[pageSlider.getCurrentPage()]);
                }
            }

            @Override
            public void onPageChosen(int i) {
            }
        });
        ScalpelFrameLayout scalpelFrameLayout = (ScalpelFrameLayout) findComponentById(ResourceTable.Id_scalpel);
        Switch switcher = (Switch) findComponentById(ResourceTable.Id_switcher);
        setShapeElement(switcher);
        switcher.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isBoolean) {
                if (isBoolean) {
                    pageSlider.setVisibility(Component.INVISIBLE);
                } else {
                    pageSlider.setVisibility(Component.VISIBLE);
                }
                scalpelFrameLayout.setLayerInteractionEnabled(isBoolean);
            }
        });
        Button button = (Button) findComponentById(ResourceTable.Id_btnFinish);

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                intent.setParam("result", "1");
                getAbility().setResult(0, intent);
                terminate();
            }
        });
    }

    private void setShapeElement(Switch switcher) {
        ShapeElement elementThumbOn = new ShapeElement();
        elementThumbOn.setShape(ShapeElement.OVAL);
        elementThumbOn.setRgbColor(RgbColor.fromArgbInt(mOnColor));
        elementThumbOn.setCornerRadius(numfifty);

        ShapeElement elementThumbOff = new ShapeElement();
        elementThumbOff.setShape(ShapeElement.OVAL);
        elementThumbOff.setRgbColor(RgbColor.fromArgbInt(mOnColor));
        elementThumbOff.setCornerRadius(numfifty);

        ShapeElement elementTrackOn = new ShapeElement();
        elementTrackOn.setShape(ShapeElement.RECTANGLE);
        elementTrackOn.setRgbColor(RgbColor.fromArgbInt(mOffColor));
        elementTrackOn.setCornerRadius(numfifty);

        ShapeElement elementTrackOff = new ShapeElement();
        elementTrackOff.setShape(ShapeElement.RECTANGLE);
        elementTrackOff.setRgbColor(RgbColor.fromArgbInt(mOffColor));
        elementTrackOff.setCornerRadius(numfifty);

        switcher.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
        switcher.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));
    }

    private StateElement trackElementInit(ShapeElement on, ShapeElement off) {
        StateElement trackElement = new StateElement();
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return trackElement;
    }

    private StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
        StateElement thumbElement = new StateElement();
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return thumbElement;
    }

    @Override
    public void onActive() {
        super.onActive();
        pageSlider.setProvider(new ShatterPagerAdapterImpl(getShatterManager()));
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /**
     * ShatterPagerAdapterImpl
     *
     * @since 2021-05-02
     */
    public class ShatterPagerAdapterImpl extends ShatterPagerAdapter {
        /**
         * ShatterPagerAdapterImpl
         *
         * @param manager
         */
        public ShatterPagerAdapterImpl(ShatterManager manager) {
            super(manager);
        }

        @Override
        public int getCount() {
            return data.length;
        }

        @Override
        public Object getItemType(int position) {
            return "Single Type"; // 只有一种类型的Item
        }

        @Override
        public Shatter createItem(Object type) {
            return new PagerShatter();
        }

        /**
         * 预加载
         *
         * @param item
         * @param position
         */
        @Override
        protected void afterInstantiateItem(Shatter item, int position) {
            super.afterInstantiateItem(item, position);
            PagerShatter shatter = (PagerShatter) item;
            shatter.handleData(data[position]);
        }
    }
}
