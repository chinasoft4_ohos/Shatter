/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.shatter;

import ohos.aafwk.content.Intent;

/**
 * 生命周期接口
 *
 * @since 2021-05-20
 */
public interface AbilityFullLifecycleListener {
    /**
     * onActStart
     *
     * @param intent
     */
    void onActStart(Intent intent);

    /**
     * onActActive
     */
    void onActActive();

    /**
     * onActInactive
     */
    void onActInactive();

    /**
     * onActForeground
     *
     * @param intent
     */
    void onActForeground(Intent intent);

    /**
     * onActBackground
     */
    void onActBackground();

    /**
     * onActStop
     */
    void onActStop();

    /**
     * onAbilityResult
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    void onAbilityResult(int requestCode, int resultCode, Intent data);

    /**
     * onActBackPressed
     */
    void onActBackPressed();
}
