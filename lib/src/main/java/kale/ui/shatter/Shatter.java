/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.shatter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSlider;

/**
 * 需要在界面销毁时把回调停止
 *
 * @author xwg
 * @since 2015-06-15
 */
public abstract class Shatter implements ShatterHostInterface, AbilityFullLifecycleListener {
    /**
     * 无布局id
     */
    public static final int NO_LAYOUT = 0;

    private Component rootView;

    private AbilitySlice abilitySlice;

    /**
     * 当前是否对用户可见
     */
    private boolean isVisibleToUser = true;

    private int containId;

    /**
     * attachAbility
     *
     * @param abilityslice
     */
    protected void attachAbility(AbilitySlice abilityslice) {
        onAttach(abilityslice);

        if (getLayoutResId() != NO_LAYOUT) {
            if (rootView instanceof PageSlider) {
                rootView = LayoutScatter.getInstance(abilityslice).parse(getLayoutResId(), null, false);
            } else if (rootView instanceof ComponentContainer) {
                Component view = LayoutScatter.getInstance(abilityslice).parse(getLayoutResId(), null, false);
                ((ComponentContainer) rootView).addComponent(view);
                rootView = view;
            } else {
                throw new IllegalArgumentException("ContainView must extends Component");
            }
        }
        onBindComponent(rootView);
    }

    /**
     * 被挂载到AbilitySlice时的回调方法
     *
     * @param abilityslice
     */
    protected void onAttach(AbilitySlice abilityslice) {
        this.abilitySlice = abilityslice;
    }

    /**
     * 得到的{@link ShatterManager}和当前Ability中的{@link ShatterManager}是同一个对象
     *
     * @return ShatterManager
     */
    public ShatterManager getShatterManager() {
        return ((ShatterHostInterface) abilitySlice).getShatterManager();
    }

    /**
     * 对用户可见的时的回调，可用作懒加载（仅在ViewPager中生效）<br>
     *
     * @param isVisible 是否可见
     */
    public void onVisibleToUser(boolean isVisible) {
        isVisibleToUser = isVisible;
    }

    /**
     * 定义后可通过{@link ShatterManager#findShatterByTag(String)}来找到{@link Shatter}
     *
     * @return 自定义的tag，默认是当前类名
     */
    public String getTag() {
        return getClass().getSimpleName();
    }

    /**
     * 获取shatter子组件的布局id
     *
     * @return layoutID
     */
    protected abstract int getLayoutResId();

    /**
     * 布局绑定的回调
     *
     * @param rootView
     */
    protected abstract void onBindComponent(Component rootView);

    // --- life ---

    /**
     * onSelfDestroy
     */
    protected void onSelfDestroy() {
    }

    @Override
    public void onActStart(Intent intent) {
    }

    @Override
    public void onActActive() {
    }

    @Override
    public void onActInactive() {
    }

    @Override
    public void onActForeground(Intent intent) {
    }

    @Override
    public void onActBackground() {
    }

    @Override
    public void onActStop() {
    }

    @Override
    public void onAbilityResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public void onActBackPressed() {
    }

    /**
     * shatter销毁
     */
    public void doDestroy() {
        onActStop();
        abilitySlice = null;
        rootView = null;
    }

    /**
     * findViewById
     *
     * @param id
     * @return Component
     */
    protected final Component findViewById(int id) {
        return rootView.findComponentById(id);
    }

    /**
     * startAbility
     *
     * @param intent
     */
    public void startAbility(Intent intent) {
        abilitySlice.startAbility(intent);
    }

    /**
     * startAbilityForResult
     *
     * @param intent
     * @param requestCode
     */
    public void startAbilityForResult(Intent intent, int requestCode) {
        abilitySlice.startAbilityForResult(intent, requestCode);
    }

    public AbilitySlice getAbilitySlice() {
        return abilitySlice;
    }

    public Component getRootView() {
        return rootView;
    }

    public void setRootView(Component rootView) {
        this.rootView = rootView;
    }

    public boolean isVisibleToUser() {
        return isVisibleToUser;
    }

    public void setContainId(int containId) {
        this.containId = containId;
    }

    public int getContainId() {
        return containId;
    }
}
