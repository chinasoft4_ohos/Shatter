package kale.ui.shatter;

import java.util.ArrayList;
import java.util.List;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;

/**
 * ShatterManager
 *
 * @author xwg
 * @since 2015-06-28
 */
public class ShatterManager {
    private List<Shatter> mShatters = new ArrayList<>();

    private AbilitySlice mAbilitySlice;

    /**
     * ShatterManager
     *
     * @param abilityslice
     * @throws IllegalArgumentException
     */
    public ShatterManager(AbilitySlice abilityslice) {
        if (abilityslice instanceof ShatterAbilitySliceInterface) {
            this.mAbilitySlice = abilityslice;
        } else {
            throw new IllegalArgumentException("AbilitySlice must be implements IShatterAbility");
        }
    }

    /**
     * 将shatter组件添加到指定容器
     *
     * @param containViewId
     * @param shatter
     * @return ShatterManager
     */
    public ShatterManager add(int containViewId, Shatter shatter) {
        shatter.setContainId(containViewId);
        return add(mAbilitySlice.findComponentById(containViewId), shatter);
    }

    /**
     * 将shatter添加到容器中
     *
     * @param containView
     * @param shatter
     * @return ShatterManager
     */
    public ShatterManager add(Component containView, Shatter shatter) {
        shatter.setRootView(containView);
        shatter.attachAbility(mAbilitySlice);
        mShatters.add(shatter);
        return this;
    }

    /**
     * 移除shatter
     *
     * @param shatter
     */
    public void remove(Shatter shatter) {
        shatter.onSelfDestroy();
        mShatters.remove(shatter);
    }

    /**
     * findShatterByTag
     *
     * @param tag
     * @param <E>
     * @return Shatter
     */
    public <E extends Shatter> E findShatterByTag(String tag) {
        for (Shatter shatter : mShatters) {
            if (tag.equals(shatter.getTag())) {
                return (E) shatter;
            }
        }
        return (E) new Shatter() {
            @Override
            protected int getLayoutResId() {
                return 0;
            }

            @Override
            protected void onBindComponent(Component rootView) {
            }
        };
    }

    /**
     * findShatterByContainViewId
     *
     * @param id
     * @return Shatter
     */
    public Shatter findShatterByContainViewId(int id) {
        for (Shatter shatter : mShatters) {
            if (shatter.getContainId() == id) {
                return shatter;
            }
        }
        return new Shatter() {
            @Override
            protected int getLayoutResId() {
                return 0;
            }

            @Override
            protected void onBindComponent(Component rootView) {
            }
        };
    }

    public List<Shatter> getShatters() {
        return mShatters;
    }

    public AbilitySlice getAbility() {
        return mAbilitySlice;
    }

    /**
     * destroy
     */
    public void destroy() {
        mShatters.clear();
        mShatters = null;
        mAbilitySlice = null;
    }
}
