/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.shatter.lifecycle;

/**
 * 页面事件监听常量
 *
 * @since 2021-05-20
 */
public class LifeEvent {
    /**
     * onStart
     */
    public static final String ON_START = "onStart";
    /**
     * onActive
     */
    public static final String ON_ACTIVE = "onActive";
    /**
     * onInactive
     */
    public static final String ON_INACTIVE = "onInactive";
    /**
     * onForeground
     */
    public static final String ON_FOREGROUND = "onForeground";
    /**
     * onBackground
     */
    public static final String ON_BACKGROUND = "onBackground";
    /**
     * onStop
     */
    public static final String ON_STOP = "onStop";
    /**
     * onAbilityResult
     */
    public static final String ON_ABILITY_RESULT = "onAbilityResult";
    /**
     * onBackPressed
     */
    public static final String ON_BACK_PRESSED = "onBackPressed";

    private LifeEvent() {
    }
}
