/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.shatter.lifecycle;

import java.util.List;

import kale.ui.shatter.Shatter;
import kale.ui.shatter.ShatterManager;
import ohos.aafwk.content.Intent;

/**
 * MethodExecutor
 *
 * @author xwg
 * @since 2017-10-23
 */
public class MethodExecutor {
    private static final int ARG_INDEX2 = 2;

    private MethodExecutor() {
    }

    /**
     * 执行事件分发
     *
     * @param methodName
     * @param manager
     * @param args
     */
    public static void scheduleMethod(String methodName, ShatterManager manager, Object[] args) {
        if (manager == null) {
            return;
        }
        List<Shatter> shatters = manager.getShatters();
        switch (methodName) {
            case LifeEvent.ON_START:
                callShatterFunc(shatters, shatter1 -> shatter1.onActStart((Intent) args[0]));
                break;
            case LifeEvent.ON_ACTIVE:
                callShatterFunc(shatters, shatter1 -> shatter1.onActActive());
                break;
            case LifeEvent.ON_INACTIVE:
                callShatterFunc(shatters, shatter1 -> shatter1.onActInactive());
                break;
            case LifeEvent.ON_STOP:
                callShatterFunc(shatters, shatter1 -> shatter1.doDestroy());
                manager.destroy();
                break;
            case LifeEvent.ON_FOREGROUND:
                callShatterFunc(shatters, shatter1 -> shatter1.onActForeground((Intent) args[0]));
                break;
            case LifeEvent.ON_BACK_PRESSED:
                callShatterFunc(shatters, Shatter::onActBackPressed);
                break;
            case LifeEvent.ON_BACKGROUND:
                callShatterFunc(shatters, shatter1 -> shatter1.onActBackground());
                break;
            case LifeEvent.ON_ABILITY_RESULT:
                callShatterFunc(shatters, shatter -> shatter.onAbilityResult(Integer.parseInt(args[0].toString()),
                        Integer.parseInt(args[1].toString()), (Intent) args[ARG_INDEX2]));
                break;
            default:
                break;
        }
    }

    private static void callShatterFunc(List<Shatter> shatters, final CallBack callback) {
        if (shatters == null || callback == null) {
            return;
        }
        for (int index = 0, size = shatters.size(); index < size; index++) {
            callback.onCall(shatters.get(index));
        }
    }

    /**
     * Callback
     *
     * @since 2021-05-02
     */
    private interface CallBack {
        void onCall(Shatter uiModule);
    }
}
