/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.shatter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class MockAbilitySlice extends AbilitySlice implements ShatterAbilitySliceInterface {

    @Override
    public ShatterManager getShatterManager() {
        return null;
    }

    @Override
    public void onStart(Intent intent) {

    }

    @Override
    public void onActive() {

    }

    @Override
    public void onInactive() {

    }

    @Override
    public void onForeground(Intent intent) {

    }

    @Override
    public void onBackground() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onAbilityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void onBackPressed() {

    }
}
