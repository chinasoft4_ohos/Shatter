/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.shatter;

import junit.framework.TestCase;
import ohos.agp.components.Component;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ShatterManagerTest extends TestCase {

    ShatterManager shatterManager;

    @Mock
    MockAbilitySlice abilitySlice;

    @Mock
    Shatter shatter;
    @Mock
    Component component;

    @Before
    public void setUp() throws Exception {
        shatterManager = new ShatterManager(abilitySlice);
        when(abilitySlice.findComponentById(Mockito.anyInt())).thenReturn(component);

        shatterManager.add(1, shatter);
    }

    @Test
    public void testAdd() {
        shatterManager.add(1, shatter);
        assertEquals(2, shatterManager.getShatters().size());
    }

    @Test
    public void testRemove() {
        shatterManager.remove(shatter);
        assertEquals(0, shatterManager.getShatters().size());
    }

    @Test
    public void testFindShatterByTag() {
        when(shatter.getTag()).thenReturn("TAG");
        Shatter shatter = shatterManager.findShatterByTag("test");
        assertNotNull(shatter);
        Shatter tagShatter = shatterManager.findShatterByTag("TAG");
        assertNotNull(tagShatter);

    }

    @Test
    public void testFindShatterByContainViewId() {
        when(shatter.getContainId()).thenReturn(1);
        Shatter shatter = shatterManager.findShatterByContainViewId(0);
        assertNotNull(shatter);
        Shatter tagShatter = shatterManager.findShatterByContainViewId(1);
        assertNotNull(tagShatter);
    }

    @Test
    public void testGetShatters() {
        assertEquals(1, shatterManager.getShatters().size());
    }


    @Test
    public void testDestroy() {
        shatterManager.destroy();
        assertNull(shatterManager.getShatters());
    }
}