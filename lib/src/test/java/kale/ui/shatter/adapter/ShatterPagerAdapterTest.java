/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.shatter.adapter;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;

public class ShatterPagerAdapterTest extends TestCase {

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
    }

    public void testIsPageMatchToObject() {
    }

    public void testCreatePageInContainer() {
    }

    public void testAfterInstantiateItem() {
    }

    public void testDestroyPageFromContainer() {
    }

    public void testOnUpdateFinished() {
    }

    public void testStartUpdate() {
    }

    public void testGetItemType() {
    }

    public void testCreateItem() {
    }
}