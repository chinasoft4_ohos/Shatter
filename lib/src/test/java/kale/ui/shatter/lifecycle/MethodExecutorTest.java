/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package kale.ui.shatter.lifecycle;

import kale.ui.shatter.Shatter;
import kale.ui.shatter.ShatterManager;
import ohos.aafwk.content.Intent;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class MethodExecutorTest {

    @Mock
    ShatterManager shatterManager;
    @Mock
    Shatter shatter;

    @Mock
    Intent intent;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();


    @Before
    public void setUp() throws Exception {
        List<Shatter> shatterList = new ArrayList<>();
        shatterList.add(shatter);
        Whitebox.setInternalState(shatterManager, "mShatters", shatterList);

        when(shatterManager.getShatters()).thenReturn(shatterList);
    }


    @Test
    public void testScheduleMethod() {
        MethodExecutor.scheduleMethod(LifeEvent.ON_ACTIVE, null, new Object[]{});
        //验证当ShatterManager为null时 onActActive 方法不会得到调用
        verify(shatter, times(0)).onActActive();

        scheduleMethod(LifeEvent.ON_START, intent);
        //验证shatter的onActStart 方法得到了调用，且传递进来的参数就是 intent对象
        verify(shatter, times(1)).onActStart(intent);

        scheduleMethod(LifeEvent.ON_ACTIVE);
        //验证onActActive 方法得到了调用
        verify(shatter).onActActive();

        scheduleMethod(LifeEvent.ON_INACTIVE);
        //验证onActActive 方法得到了调用
        verify(shatter).onActInactive();

        scheduleMethod(LifeEvent.ON_FOREGROUND, intent);
        //验证shatter的onActStart 方法得到了调用，不对入参校验
        verify(shatter, times(1)).onActForeground(Mockito.any());

        scheduleMethod(LifeEvent.ON_BACKGROUND);
        //验证onActActive 方法得到了调用
        verify(shatter).onActBackground();

        scheduleMethod(LifeEvent.ON_ABILITY_RESULT, 100, 1, intent);
        //验证shatter的onActStart 方法得到了调用
        verify(shatter, times(1)).onAbilityResult(100, 1, intent);

        scheduleMethod(LifeEvent.ON_BACK_PRESSED);
        //验证onActActive 方法得到了调用
        verify(shatter).onActBackPressed();

        scheduleMethod(LifeEvent.ON_STOP);
        //doDestroy 方法得到了调用
        verify(shatter).doDestroy();
        verify(shatterManager, times(8)).getShatters();
        verify(shatterManager, times(1)).destroy();
    }

    private void scheduleMethod(String eventName, Object... args) {
        MethodExecutor.scheduleMethod(eventName, shatterManager, args);
    }
}